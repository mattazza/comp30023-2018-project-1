/* 832993 
Matthew Azzato */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

/*Server receives a wget
1. Must check within the server (aka the files whether there is such file labelled) - readfile
2. Read the request line by line through fopen() and fread()
3. Extract the types
4. Send it back to the client
*/

#define mime_html "Content-Type: text/html"
#define mime_jpeg "Content-Type: image/jpeg"
#define mime_css "Content-Type: text/css"
#define mime_javascript "Content-Type: text/javascript"
/*Read the requested file path - if its there: respond 200 OK, otherwise respond 404 
  "200 OK" "404 Not Found" */
char* checkFilePath(char* filePath, char* buffer) {
    
   //Turn firstline of get request into just the file format
   
  
    /* Under <unistd.h> access() checks if a file path exists
    -1 if does not exist */
    if( access( ("%s%s",filePath,buffer), F_OK ) != -1 ) {
        return "HTTP/1.1 200 OK";
      } 
    return "HTTP/1.1 404 Not Found";
}

/*Determine MIME type for the system 
Determined by reading the file extension at the end after the '.'
*/  

char * checkMimeType(char *filename) {
  
  //Possible formats:
  char* html = ".html";
  char* jpeg = ".jpg";
  char* css = ".css";
  char* javascript = ".js";


  //Need to find where the extension lies
	char *ext = strrchr(filename, '.');
  
  
  //Return null if the extension is not used
	char *mime_type = NULL;

  //Compare the extensions with the string
  if (strcmp(ext, html) == 0) {
    return mime_html;
  }
  
  if (strcmp(ext, jpeg) == 0) { 
    return mime_jpeg;
  }
  
  if (strcmp(ext, css) == 0) {
    return mime_css;
  }
  
  if (strcmp(ext, javascript) == 0) {
    return mime_javascript;
  }
  
  //Return null byte if not a valid extension
	return mime_type;
}

//Mainly copied from server.c Lab5
int main(int argc, char **argv)
{
	int sockfd, newsockfd, portno;// clilen;
	char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;
	socklen_t clilen;
	int n;
  char* pathtoroot;
  
   
  
	if (argc != 3) 
	{
		fprintf(stderr,"ERROR, Invalid arguments\n");
		exit(1);
	}

	 /* Create TCP socket */
	
	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	if (sockfd < 0) 
	{
		perror("ERROR opening socket");
		exit(1);
	}

	
	bzero((char *) &serv_addr, sizeof(serv_addr));

	portno = atoi(argv[1]);
	pathtoroot = argv[2];
  
	/* Create address we're going to listen on (given port number)
	 - converted to network byte order & any IP address for 
	 this machine */
	
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);  // store in machine-neutral format

	 /* Bind address to the socket */
	
	if (bind(sockfd, (struct sockaddr *) &serv_addr,
			sizeof(serv_addr)) < 0) 
	{
		perror("ERROR on binding");
		exit(1);
	}
	
	/* Listen on socket - means we're ready to accept connections - 
	 incoming connection requests will be queued */
	
	listen(sockfd,5);
	
	clilen = sizeof(cli_addr);

	/* Accept a connection - block until a connection is ready to
	 be accepted. Get back a new file descriptor to communicate on. */

	newsockfd = accept(	sockfd, (struct sockaddr *) &cli_addr, 
						&clilen);

	if (newsockfd < 0) 
	{
		perror("ERROR on accept");
		exit(1);
	}
	
	bzero(buffer,256);

	/* Read characters from the connection,
		then process */
	
	n = read(newsockfd,buffer,255);

	if (n < 0) 
	{
		perror("ERROR reading from socket");
		exit(1);
	}
  
  //Send back response header, mimetype to the 
  //Make a buffer index for only the page itself
  //Stop until reaches the first "/" character
  char* buffer_index;
  buffer_index = malloc(128*sizeof(char));
  buffer_index = strchr(buffer, '/');
  buffer_index = strchr(buffer_index, ' ');
  char* new_buffer;
  
  //New buffer will be the "/filename.type" in the buffer GET request
  new_buffer = malloc(128*sizeof(char));
  for (int i=0; i<strlen(buffer_index)-2; i++) {
    new_buffer[i] = buffer_index[i];
  }
  
 
  char* header = checkFilePath(pathtoroot, new_buffer);
  char* mimeType = checkMimeType(new_buffer);
	n = write(newsockfd,("%s\n%s",header,mimeType),18);
	
	if (n < 0) 
	{
		perror("ERROR writing to socket");
		exit(1);
	}

	/* close socket */
	
	close(sockfd);
	
	return 0; 
}